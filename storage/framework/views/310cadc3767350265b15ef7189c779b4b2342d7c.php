<div class="row">
    <div class="form-group col-md-6 <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
        <?php echo Html::decode(Form::label('name', 'Full Name <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ); ?>

        <div class="col-md-12">
            <?php echo Form::text('name', null , ['class' => 'form-control'] ); ?>

            <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

        </div>
    </div> 
</div>

<div class="row">
    <div class="form-group col-md-6 <?php echo e($errors->has('dob') ? 'has-error' : ''); ?>">
        <?php echo Html::decode(Form::label('dob', 'Date of Birth <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ); ?>

        <div class="col-md-12">
            <?php echo Form::text('dob', null, ['class' => 'form-control datepicker']); ?>

            <?php echo $errors->first('dob', '<p class="help-block">:message</p>'); ?>

        </div>
    </div>

    <div class="form-group col-md-6 <?php echo e($errors->has('type') ? 'has-error' : ''); ?>">
        <?php echo Html::decode(Form::label('type', 'Class', ['class' => 'control-label col-md-12']) ); ?>

        <div class="col-md-12">
            <?php echo Form::select('type', ['father' => 'Father', 'mother' => 'Mother'], null, ['class' => 'form-control']); ?>

            <?php echo $errors->first('type', '<p class="help-block">:message</p>'); ?>

        </div>
    </div>
</div> 

<div class="row">
    <div class="form-group col-md-12">
        <div class="col-md-12"> 
            <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Submit', ['class' => 'btn btn-primary']); ?>

        </div>
    </div>
</div>
<?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
<script type="text/javascript">
    $(document).ready(function () {
        // body...
        $('.datepicker').datepicker({
            endDate: '-1d',
            orientation: 'bottom',
            format: 'yyyy-mm-dd'
        });
    })
</script>