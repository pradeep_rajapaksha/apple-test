<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<?php $__env->startSection('content_header'); ?>
    <!-- <div class="container">
        <h3>Suppliers</h3>
    </div> -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="box box-primary">

            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Class Details
                            <div class="pull-right">
                                <a class="btn btn-default" href="<?php echo e(url('classes')); ?>">Back</a>
                            </div>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="box-body im-box-body">
                <!-- <div class="well well-lg"> -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped mb-0">
                                <tr>
                                    <th scope="row">Name</th>
                                    <td><?php echo e($class->name); ?></td>
                                    
                                    <th scope="row">Year</th>
                                    <td><?php echo e($class->year); ?></td>
                                </tr>
                                
                                <tr>
                                    <th scope="row"># of Students</th>
                                    <td colspan="3"><?php echo e(count($class->students)); ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <!-- </div> -->
            </div>

            <hr>

            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Class Students Details
                        </h4>
                    </div>
                </div>
            </div>

            <div class="box-body im-box-body">
                <!-- <div class="well well-lg"> -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th scope="row">#</th>
                                        <th>Name</th>
                                        <th>City</th>
                                        <th>Date of Birth</th>
                                    </tr>
                                </thead>
                                <?php foreach ($class->students as $key => $student): ?>
                                    <tr>
                                        <td scope="row"><?php echo e($key+1); ?></td>
                                        <td>
                                            <a href="<?php echo e(url('students/'.$student->id)); ?>"><?php echo e($student->name); ?></a>
                                        </td>
                                        <td><?php echo e($student->city); ?></td>
                                        <td><?php echo e($student->dob); ?></td>
                                    </tr>
                                <?php endforeach ?>
                            </table>
                        </div>
                    </div>
                <!-- </div> -->
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>