<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<?php $__env->startSection('content_header'); ?>
    <!-- <div class="container">
        <h3>Suppliers</h3>
    </div> -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="box box-primary">

            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Parent Details
                            <div class="pull-right">
                                <a class="btn btn-default" href="<?php echo e(url('parents')); ?>">Back</a>
                            </div>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="box-body im-box-body">
                <!-- <div class="well well-lg"> -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped mb-0">
                                <tr>
                                    <th scope="row">Name</th>
                                    <td><?php echo e($parent->name); ?></td>
                                    
                                    <th scope="row">Date of Birth</th>
                                    <td><?php echo e($parent->dob); ?></td>
                                </tr>
                                
                                <tr>
                                    <th scope="row">Childs</th>
                                    <td colspan="3">
                                        <?php $__currentLoopData = $parent->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <a href="<?php echo e(url('students/'.$child->id)); ?>"> <?php echo e($child->name); ?> </a> 
                                            <?php if (end($parent->childs) !== $child): ?>
                                                , 
                                            <?php endif ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <!-- </div> -->
            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>