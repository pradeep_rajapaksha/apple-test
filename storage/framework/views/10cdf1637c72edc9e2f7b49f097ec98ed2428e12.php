<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<?php $__env->startSection('content_header'); ?>
    <!-- <div class="container">
        <h3>Suppliers</h3>
    </div> -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="box box-primary">

            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Student Details
                            <div class="pull-right">
                                <a class="btn btn-default" href="<?php echo e(url('students')); ?>">Back</a>
                            </div>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="box-body im-box-body">
                <!-- <div class="well well-lg"> -->
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-striped mb-0">
                                <tr>
                                    <th scope="row">Name</th>
                                    <td><?php echo e($student->name); ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Class</th>
                                    <td><?php echo e($student->class->name); ?></td>
                                </tr>

                                <tr>
                                    <th scope="row">Date of Birth</th>
                                    <td><?php echo e($student->dob); ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Address</th>
                                    <td><?php echo e($student->address); ?></td>
                                </tr>

                                <tr>
                                    <th scope="row">City</th>
                                    <td><?php echo e($student->city); ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">City Coordinates</th>
                                    <td><?php echo e($student->city_coordinates); ?></td>
                                </tr>
                                
                                <tr>
                                    <th scope="row">Parent</th>
                                    <td>
                                        <?php $__currentLoopData = $student->parent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <a href="<?php echo e(url('parents/'.$parent->id)); ?>"> <?php echo e($parent->name); ?> </a> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-md-6">
                            <iframe 
                                width="100%" 
                                height="300" 
                                src="https://maps.google.com/maps?q=<?php echo e($student->city_coordinates); ?>&hl=es;z=14&amp;output=embed" 
                                frameborder="0" 
                                scrolling="no" 
                                marginheight="0" 
                                marginwidth="0">
                            </iframe>
                            <br><br>
                        </div>
                    </div>
                <!-- </div> -->
            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>