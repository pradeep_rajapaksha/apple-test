<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('adminlte::page')
@section('content_header')
    <!-- <div class="container">
        <h3>Suppliers</h3>
    </div> -->
@stop

@section('content')
    <div class="container-fluid">
        <div class="box box-primary">

            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Class Details
                            <div class="pull-right">
                                <a class="btn btn-default" href="{{url('classes')}}">Back</a>
                            </div>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="box-body im-box-body">
                <!-- <div class="well well-lg"> -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped mb-0">
                                <tr>
                                    <th scope="row">Name</th>
                                    <td>{{$class->name}}</td>
                                    
                                    <th scope="row">Year</th>
                                    <td>{{$class->year}}</td>
                                </tr>
                                
                                <tr>
                                    <th scope="row"># of Students</th>
                                    <td colspan="3">{{count($class->students)}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <!-- </div> -->
            </div>

            <hr>

            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Class Students Details
                        </h4>
                    </div>
                </div>
            </div>

            <div class="box-body im-box-body">
                <!-- <div class="well well-lg"> -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th scope="row">#</th>
                                        <th>Name</th>
                                        <th>City</th>
                                        <th>Date of Birth</th>
                                    </tr>
                                </thead>
                                <?php foreach ($class->students as $key => $student): ?>
                                    <tr>
                                        <td scope="row">{{$key+1}}</td>
                                        <td>
                                            <a href="{{url('students/'.$student->id)}}">{{$student->name}}</a>
                                        </td>
                                        <td>{{$student->city}}</td>
                                        <td>{{$student->dob}}</td>
                                    </tr>
                                <?php endforeach ?>
                            </table>
                        </div>
                    </div>
                <!-- </div> -->
            </div>
        </div>
    </div>

@stop

<script src="{{asset('js/app.js')}}"></script>