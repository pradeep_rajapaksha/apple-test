<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('name') ? 'has-error' : ''}}">
        {!!  Html::decode(Form::label('name', 'Class Name <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!!  Form::text('name', null , ['class' => 'form-control'] ) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div> 
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('year') ? 'has-error' : ''}}">
        {!! Html::decode(Form::label('year', 'Year <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!! Form::text('year', null, ['class' => 'form-control datepicker datepicker-year-only']) !!}
            {!! $errors->first('year', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div> 

<div class="row">
    <div class="form-group col-md-12">
        <div class="col-md-12"> 
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Submit', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
</div>
<!-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif -->
<script type="text/javascript">
    $(document).ready(function () {
        // body...
        $('.datepicker.datepicker-year-only').datepicker({
            endDate: '-0y',
            orientation: 'bottom',
            format: 'yyyy',
            viewMode: 'years', 
            minViewMode: 'years'
        });
    })
</script>