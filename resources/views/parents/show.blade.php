<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('adminlte::page')
@section('content_header')
    <!-- <div class="container">
        <h3>Suppliers</h3>
    </div> -->
@stop

@section('content')
    <div class="container-fluid">
        <div class="box box-primary">

            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Parent Details
                            <div class="pull-right">
                                <a class="btn btn-default" href="{{url('parents')}}">Back</a>
                            </div>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="box-body im-box-body">
                <!-- <div class="well well-lg"> -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped mb-0">
                                <tr>
                                    <th scope="row">Name</th>
                                    <td>{{$parent->name}}</td>
                                    
                                    <th scope="row">Date of Birth</th>
                                    <td>{{$parent->dob}}</td>
                                </tr>
                                
                                <tr>
                                    <th scope="row">Childs</th>
                                    <td colspan="3">
                                        @foreach($parent->childs as $child)
                                            <a href="{{ url('students/'.$child->id) }}"> {{ $child->name }} </a> 
                                            <?php if (end($parent->childs) !== $child): ?>
                                                , 
                                            <?php endif ?>
                                        @endforeach
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <!-- </div> -->
            </div>

        </div>
    </div>

@stop

<script src="{{asset('js/app.js')}}"></script>