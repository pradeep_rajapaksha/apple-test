<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('name') ? 'has-error' : ''}}">
        {!!  Html::decode(Form::label('name', 'Full Name <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!!  Form::text('name', null , ['class' => 'form-control'] ) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div> 
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('dob') ? 'has-error' : ''}}">
        {!! Html::decode(Form::label('dob', 'Date of Birth <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!! Form::text('dob', null, ['class' => 'form-control datepicker']) !!}
            {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group col-md-6 {{ $errors->has('type') ? 'has-error' : ''}}">
        {!! Html::decode(Form::label('type', 'Class', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!! Form::select('type', ['father' => 'Father', 'mother' => 'Mother'], null, ['class' => 'form-control']) !!}
            {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div> 

<div class="row">
    <div class="form-group col-md-12">
        <div class="col-md-12"> 
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Submit', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<script type="text/javascript">
    $(document).ready(function () {
        // body...
        $('.datepicker').datepicker({
            endDate: '-1d',
            orientation: 'bottom',
            format: 'yyyy-mm-dd'
        });
    })
</script>