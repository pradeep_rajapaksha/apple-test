<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')

    <div class="container-fluid">
    	<div class="row">
	        <div class="col-md-6 ">
		        <div class="box box-primary">
		            <div class="box-header">
		                <div class="row">
		                    <div class="col-md-12">
		                        <h4>
		                            Students:: Age filter
		                        </h4>
		                    </div>
		                </div>
		            </div>
		            <div class="box-body im-box-body">
		                <div class="row">
		                    <div class="col-md-12">
		                    	<table class="agefilter">
		                    		<tr>
		                    			<td align="right">
		                    				<span>Min: <input type="search" name="agemin" id="agemin" class="agefilter-input" value="18" /></span>
		                    				<span>Max: <input type="search" name="agemax" id="agemax" class="agefilter-input" value="" /></span>
		                    			</td>
		                    		</tr>
		                    	</table>
		                        <table id="students-age-filter" class="table table-striped table-bordered" cellspacing="0" width="100%">
		                            <thead>
		                            <tr>
		                                <th>ID</th>
		                                <th>Name</th> 
		                                <th>Age</th> 
		                                <th>Class</th>
		                            </tr>
		                            </thead>
		                        </table>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
	        <div class="col-md-6 ">
		        <div class="box box-primary">
		            <div class="box-header">
		                <div class="row">
		                    <div class="col-md-12">
		                        <h4>
		                            Students:: Class - Year filter
		                        </h4>
		                    </div>
		                </div>
		            </div>
		            <div class="box-body im-box-body">
		                <div class="row">
		                    <div class="col-md-12">
		                    	<table class="classyearfilter">
		                    		<tr>
		                    			<td align="right">
		                    				<span>Class: <input type="search" name="class" id="class" class="classyearfilter-input" value="8" /></span>
		                    				<span>Year: <input type="search" name="year" id="year" class="classyearfilter-input" value="2010" /></span>
		                    			</td>
		                    		</tr>
		                    	</table>
		                        <table id="students-class-year-filter" class="table table-striped table-bordered" cellspacing="0" width="100%">
		                            <thead>
		                            <tr>
		                                <th>ID</th>
		                                <th>Name</th> 
		                                <th>Class</th> 
		                                <th>Year</th> 
		                            </tr>
		                            </thead>
		                        </table>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Students:: Student/Parent Age filter
                        </h4>
                    </div>
                </div>
            </div>
            <div class="box-body im-box-body">
                <div class="row">
                    <div class="col-md-12">
                    	<table class="studentparentagefilter">
                    		<tr>
                    			<td align="right">
                    				<span>Student: <input type="search" name="agestudent" id="agestudent" class="studentparentagefilter-input" value="16" /></span>
                    				<span>Parent: <input type="search" name="ageparent" id="ageparent" class="studentparentagefilter-input" value="50" /></span>
                    			</td>
                    		</tr>
                    	</table>
                        <table id="students-student-parent-age-filter" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Student Name</th> 
                                <th>Student Age</th> 
                                <th>Class</th> 
                                <th>Parent Name</th> 
                                <th>Parent Age</th> 
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

<style type="text/css">
    *, table { font-size: 14px; }
    .business{  display: none; }
    .im-box-body { width: 100%; display: block; margin: auto; }
    .box { padding: 0 15px !important; }
    .box-header h4 { line-height: 34px; font-size: 16px; margin-bottom: 0; }
    .box-header h4 strong { font-size: 16px; }
    .dataTables_paginate span .paginate_button { font-size: 12px; line-height: 1.5; box-shadow: none; padding: 4px 10px !important; }

	table.agefilter, table.classyearfilter, table.studentparentagefilter {
		position: inherit;
	    z-index: 1;
	    width: 100%; 
	    margin-bottom: -28px;
	}
	input.agefilter-input, input.classyearfilter-input, input.studentparentagefilter-input {
	    border-radius: 3px;
	    border: 1px solid #ddd;
	    padding: 3px;
	    line-height: 12px;
	    width: 75px;
	    text-align: center;
	    margin-left: 0.5em;
	    -webkit-appearance: none;
	}
</style>

<script src="{{asset('js/app.js')}}"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	let ageFilterDataTable;
	let classYearFilterDataTable;
	let studentParentAgeFilterDataTable;
    let students = {
        load: function () {
            students.bind();
            students.ageFilter();
            students.classYearFilter();
            students.studentParentAgeFilter();
        },
        bind: function () {  },
        ageFilter: function () {
        	return ageFilterDataTable = $('#students-age-filter').dataTable({
	        	"bFilter": false,
	        	"bInfo": false,
	            "ajax": {
	                "url": '/reports/_ajax/report01?agemax=&agemin=18',
	                "dataSrc": "students"
	            },
	            "columns": [
	                {"data": "row", "width": "5%"},
	                {"data": "name", "width": "30%"},
	                {"data": "age", "width": "15%"},
	                {"data": "class", "width": "15%"}
	            ],
	        });
        },
        classYearFilter: function () {
        	return classYearFilterDataTable = $('#students-class-year-filter').dataTable({
	        	"bFilter": false,
	        	"bInfo": false,
	            "ajax": {
	                "url": '/reports/_ajax/report02?class=8&year=2010',
	                "dataSrc": "students"
	            },
	            "columns": [
	                {"data": "row", "width": "5%"},
	                {"data": "name", "width": "30%"},
	                {"data": "class", "width": "15%"},
	                {"data": "year", "width": "15%"}
	            ],
	        });
        },
        studentParentAgeFilter: function () {
        	return studentParentAgeFilterDataTable = $('#students-student-parent-age-filter').dataTable({
	        	"bFilter": false,
	        	"bInfo": false,
	            "ajax": {
	                "url": '/reports/_ajax/report03?agestudent=16&ageparent=50',
	                "dataSrc": "students"
	            },
	            "columns": [
	                {"data": "row", "width": "5%"},
	                {"data": "name", "width": "25%"},
	                {"data": "agestudent", "width": "15%"},
	                {"data": "class", "width": "15%"},
	                {"data": "parent_name", "width": "15%"},
	                {"data": "ageparent", "width": "15%"}
	            ],
	        });
        }
    };

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        students.load(); 

        $('#agemin, #agemax').keyup( function() {
        	var _agemin = $('#agemin').val(); 
			var _agemax = $('#agemax').val(); 
			ageFilterDataTable.api().ajax.url( '/reports/_ajax/report01?agemax='+_agemax+'&agemin='+_agemin+'' ).load(); 
	    });
        $('#class, #year').keyup( function() {
			var _class = $('#class').val(); 
        	var _year = $('#year').val(); 
			classYearFilterDataTable.api().ajax.url( '/reports/_ajax/report02?class='+_class+'&year='+_year+'' ).load(); 
	    });
        $('#agestudent, #ageparent').keyup( function() {
			var _agestudent = $('#agestudent').val(); 
        	var _ageparent = $('#ageparent').val(); 
			studentParentAgeFilterDataTable.api().ajax.url( '/reports/_ajax/report03?agestudent='+_agestudent+'&ageparent='+_ageparent+'' ).load(); 
	    });
    });
</script>
