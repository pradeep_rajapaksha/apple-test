<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('adminlte::page')
@section('content_header')
    <!-- <div class="container">
        <h3>Suppliers</h3>
    </div> -->
@stop

@section('content')
    <div class="container-fluid">
        <div class="box box-primary">

            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Student Details
                            <div class="pull-right">
                                <a class="btn btn-default" href="{{url('students')}}">Back</a>
                            </div>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="box-body im-box-body">
                <!-- <div class="well well-lg"> -->
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-striped mb-0">
                                <tr>
                                    <th scope="row">Name</th>
                                    <td>{{$student->name}}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Class</th>
                                    <td>{{$student->class->name}}</td>
                                </tr>

                                <tr>
                                    <th scope="row">Date of Birth</th>
                                    <td>{{$student->dob}}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Address</th>
                                    <td>{{$student->address}}</td>
                                </tr>

                                <tr>
                                    <th scope="row">City</th>
                                    <td>{{$student->city}}</td>
                                </tr>
                                <tr>
                                    <th scope="row">City Coordinates</th>
                                    <td>{{$student->city_coordinates}}</td>
                                </tr>
                                
                                <tr>
                                    <th scope="row">Parent</th>
                                    <td>
                                        @foreach($student->parent as $parent)
                                            <a href="{{ url('parents/'.$parent->id) }}"> {{ $parent->name }} </a> 
                                        @endforeach
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-md-6">
                            <iframe 
                                width="100%" 
                                height="300" 
                                src="https://maps.google.com/maps?q={{$student->city_coordinates}}&hl=es;z=14&amp;output=embed" 
                                frameborder="0" 
                                scrolling="no" 
                                marginheight="0" 
                                marginwidth="0">
                            </iframe>
                            <br><br>
                        </div>
                    </div>
                <!-- </div> -->
            </div>

        </div>
    </div>

@stop

<script src="{{asset('js/app.js')}}"></script>