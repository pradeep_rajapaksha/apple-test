<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('name') ? 'has-error' : ''}}">
        {!!  Html::decode(Form::label('name', 'Full Name <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!!  Form::text('name', null , ['class' => 'form-control'] ) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div> 
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('dob') ? 'has-error' : ''}}">
        {!! Html::decode(Form::label('dob', 'Date of Birth <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!! Form::text('dob', null, ['class' => 'form-control datepicker']) !!}
            {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group col-md-6 {{ $errors->has('address') ? 'has-error' : ''}}">
        {!! Html::decode(Form::label('address', 'Address <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!! Form::text('address', null , ['class' => 'form-control']) !!}
            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div> 

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('city') ? 'has-error' : ''}}">
        {!! Html::decode(Form::label('city', 'City <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!! Form::text('city', null, ['class' => 'form-control']) !!}
            {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group col-md-6 {{ $errors->has('city_coordinates') ? 'has-error' : ''}}">
        {!! Html::decode(Form::label('city_coordinates', 'City Coordinates', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!! Form::text('city_coordinates', null , ['class' => 'form-control', 'readonly' => 'readonly']) !!}
            {!! $errors->first('city_coordinates', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div> 
<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('class_id') ? 'has-error' : ''}}">
        {!! Html::decode(Form::label('class_id', 'Class', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!! Form::select('class_id', $classes, null, ['class' => 'form-control']) !!}
            {!! $errors->first('class_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group col-md-6 {{ $errors->has('parent_id') ? 'has-error' : ''}}">
        {!! Html::decode(Form::label('parent_id', 'Parent <span class="req_star">*</span>', ['class' => 'control-label col-md-12']) ) !!}
        <div class="col-md-12">
            {!! Form::select('parent_id', $parents, null, ['class' => 'form-control']) !!}
            {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div> 

<div class="row">
    <div class="form-group col-md-12">
        <div class="col-md-12"> 
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Submit', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
</div>
<!-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // body...
        $('.datepicker').datepicker({
            endDate: '-1d',
            orientation: 'bottom',
            format: 'yyyy-mm-dd'
        });

        $(document).on('change', '#city', function() {
            var city = $(this).val();
            // console.log(city); 
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': city}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    console.log(latitude, longitude);
                    $('#city_coordinates').val(latitude+','+longitude)
                } 
            }); 
        });
    });
</script>