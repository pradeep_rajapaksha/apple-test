<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_admin = \App\Role::where('name', 'admin')->first();
	    $role_user  = \App\Role::where('name', 'user')->first();

	    $employee = new \App\User();
	    $employee->name = 'Pradeep Rajapaksha';
	    $employee->email = 'pradeepprasanna.rajapaksha4@gmail.com';
	    $employee->password = bcrypt('pass@123');
	    $employee->save();
	    $employee->roles()->attach($role_admin);

	    $manager = new \App\User();
	    $manager->name = 'System User';
	    $manager->email = 'user@appletest.com';
	    $manager->password = bcrypt('user@123');
	    $manager->save();
	    $manager->roles()->attach($role_user);
    }
}
