-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for appletest
CREATE DATABASE IF NOT EXISTS `appletest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `appletest`;

-- Dumping structure for table appletest.classes
CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` year(4) NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.classes: ~3 rows (approximately)
DELETE FROM `classes`;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` (`id`, `name`, `year`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'Grade 01', '2018', 'active', NULL, NULL, NULL),
	(2, 'Grade 02', '2018', 'active', NULL, NULL, '2018-11-05 10:44:05'),
	(3, 'Grade 03', '2018', 'active', NULL, '2018-11-05 10:43:52', '2018-11-05 10:44:33');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;

-- Dumping structure for table appletest.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.migrations: ~9 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_11_05_051000_create_sessions_table', 1),
	(4, '2018_11_05_054028_create_students_table', 1),
	(5, '2018_11_05_054316_create_parents_table', 1),
	(6, '2018_11_05_054331_create_classes_table', 1),
	(7, '2018_11_05_060356_create_student_parent_table', 1),
	(8, '2018_11_05_152505_create_user_role_table', 2),
	(9, '2018_11_05_154826_create_roles_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table appletest.parents
CREATE TABLE IF NOT EXISTS `parents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('mother','father') COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.parents: ~3 rows (approximately)
DELETE FROM `parents`;
/*!40000 ALTER TABLE `parents` DISABLE KEYS */;
INSERT INTO `parents` (`id`, `name`, `type`, `dob`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'Mr.Pradeep Rajapaksha', 'father', '1992-07-15', 'active', NULL, '2018-11-05 09:23:36', '2018-11-05 09:25:03'),
	(2, 'Mr. Ishantha Dhanushke', 'father', '1992-01-27', 'active', NULL, '2018-11-05 10:11:46', '2018-11-05 10:11:46'),
	(3, 'Mrs.Rukshani Dalugama', 'mother', '1991-12-03', 'active', NULL, '2018-11-05 10:12:15', '2018-11-05 10:12:15');
/*!40000 ALTER TABLE `parents` ENABLE KEYS */;

-- Dumping structure for table appletest.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table appletest.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.roles: ~2 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Administrator', '2018-11-05 16:14:24', '2018-11-05 16:14:24'),
	(2, 'user', 'User', '2018-11-05 16:14:24', '2018-11-05 16:14:24');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table appletest.role_user
CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.role_user: ~3 rows (approximately)
DELETE FROM `role_user`;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` (`id`, `role_id`, `user_id`) VALUES
	(1, 2, 3),
	(2, 1, 1),
	(3, 2, 4);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;

-- Dumping structure for table appletest.sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.sessions: ~0 rows (approximately)
DELETE FROM `sessions`;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Dumping structure for table appletest.students
CREATE TABLE IF NOT EXISTS `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` int(11) NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_coordinates` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.students: ~3 rows (approximately)
DELETE FROM `students`;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` (`id`, `name`, `class_id`, `dob`, `address`, `city`, `city_coordinates`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(2, 'Binara Sidhath Rajapaksha', 2, '2008-07-10', 'B/79, Sama Mawatha, Maligawila', 'Monaragala', '6.7563232,81.25188330000003', 'active', NULL, '2018-11-05 08:01:34', '2018-11-05 16:43:24'),
	(3, 'Dingiri Dalugama', 1, '2010-02-10', 'Kelaniya', 'Kelaniya', '6.951784600000001,79.91329910000002', 'active', NULL, '2018-11-05 10:13:24', '2018-11-05 15:00:05'),
	(4, 'Somapala Dalugama', 1, '2012-04-06', 'Kelaniya', 'Kelaniya', '', 'active', NULL, '2018-11-05 10:13:54', '2018-11-05 10:13:54');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;

-- Dumping structure for table appletest.student_parent
CREATE TABLE IF NOT EXISTS `student_parent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.student_parent: ~3 rows (approximately)
DELETE FROM `student_parent`;
/*!40000 ALTER TABLE `student_parent` DISABLE KEYS */;
INSERT INTO `student_parent` (`id`, `student_id`, `parent_id`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, NULL, NULL),
	(2, 3, 3, NULL, NULL),
	(4, 4, 3, NULL, NULL);
/*!40000 ALTER TABLE `student_parent` ENABLE KEYS */;

-- Dumping structure for table appletest.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table appletest.users: ~3 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'Pradeep Rajapaksha', 'pradeepprasanna.rajapaksha4@gmail.com', '$2y$10$i99uitEUmpfqkauYsOQ09OpjZCprbK/KGNzjVafGO6PBnASH6Ln8q', 'GFnlJ9ZYxoHxfUi6f3O6r8vUJcxPjuktGEp1OuFckPz1SNvWCmSbr0SseAy3', NULL, '2018-11-05 06:10:11', '2018-11-05 06:10:11'),
	(3, 'System User 01', 'user@appletest.com', '$2y$10$F9Wfc6cpLB1jEDXbxvmJOODQEe6DZ7H8zvLwsOR9LyafP5roWmru2', 't9aqSTmeiQ2Cpp4IPlzyJSRbxs0oENahDCDgTYxq1TKdEtj8PwxNKmuhA9Pj', NULL, '2018-11-05 16:16:44', '2018-11-05 16:58:59'),
	(4, 'System User 02', 'user2@appletest.com', '$2y$10$1Sn7lwPX.5QvpuX./kkbEO2DtvYxDasJss6LOxBoLUmb..X/zOuLu', 'Wk9g24ieSTjt2P9PRXEVThgiX0bPE5IbOXsTiHnIW0jrLOK13HWg8yA6ibKt', NULL, '2018-11-05 16:58:49', '2018-11-05 16:58:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
