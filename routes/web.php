<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();
// Route::get('/register', function () {
//     return redirect()->route('login');
// });
Route::get('/home', function () {
    return redirect()->route('dashboard');
});

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::group(['prefix' => ''], function() {

	Route::get('/profile','ProfileController@index')->name('user.profile');
	Route::get('/profile/edit','ProfileController@edit')->name('user.profile.edit');
	Route::patch('/profile/edit','ProfileController@update')->name('user.profile.update');
	
	Route::get('/profile/psw/change','ProfileController@change_psw')->name('user.profile.password.change');
	Route::patch('/profile/psw/change','ProfileController@update_psw')->name('user.profile..update');

	Route::get('/profile/_ajax/image','ProfileController@profile_image')->name('user.profile.image');
	Route::post('/profile/_ajax/image','ProfileController@profile_image')->name('user.profile.image.upload');

	Route::get('/staff/_ajax', 'UserController@_ajaxData')->name('staff._ajax');
	Route::get('/staff/{id}/delete','UserController@destroy')->name('staff.delete');
	Route::resource('/staff', 'UserController');

	Route::get('/students/_ajax', 'StudentController@_ajaxData')->name('students._ajax');
	Route::get('/students/{id}/delete','StudentController@destroy')->name('students.delete');
	Route::resource('/students', 'StudentController');

	Route::get('/parents/_ajax', 'ParentController@_ajaxData')->name('parents._ajax');
	Route::get('/parents/{id}/delete','ParentController@destroy')->name('parents.delete');
	Route::resource('/parents', 'ParentController');

	Route::get('/classes/_ajax', 'ClassController@_ajaxData')->name('classes._ajax');
	Route::get('/classes/{id}/delete','ClassController@destroy')->name('classes.delete');
	Route::resource('/classes', 'ClassController');

	Route::get('/reports/_ajax/report01', 'ReportsController@_ajaxReport1')->name('report._ajax_1');
	Route::get('/reports/_ajax/report02', 'ReportsController@_ajaxReport2')->name('report._ajax_2');
	Route::get('/reports/_ajax/report03', 'ReportsController@_ajaxReport3')->name('report._ajax_3');
});
