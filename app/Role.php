<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * The attributes that should be valid for create.
     *
     * @var array
     */
    public static $rules = [
        'name'=> 'required',
        'password'  => 'description',
    ];

	/**
     * The role that belong to the users.
     */
    public function users() {
	  	return $this->belongsToMany('\App\User');
	}
}
