<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Parents; 

class ParentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
        return view('parents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        // 
        return view('parents.create', compact('classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        //
        $this->validate($request, \App\Parents::$rules);
        
        $parent = new \App\Parents();
        $parent->name  = $request->name;
        $parent->dob   = $request->dob;
        $parent->type  = $request->type;
        if ($parent->save()) {
            // 
            return redirect('parents')->with('flash_message_success', 'Parent created successfully!');
        }
        
        return redirect()->back()->withInput()->with('flash_message_error', 'Parent couldn`t create successfully! Please chack entered data.');
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $parent = \App\Parents::findOrFail($id);
        // dd($parent->childs);
        return view('parents.show', compact('parent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 

        $parent = \App\Parents::findOrFail($id);
        return view('parents.edit', compact('parent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        // 
        \DB::beginTransaction();
            try { 
                $parent = \App\Parents::find($id);
                $parent->name              = $request->name;
                $parent->dob               = $request->dob;
                $parent->type          = $request->type;
                $parent->save();
                // 
            } catch (Exception $e) { 
                \DB::rollBack(); 
                return redirect('')->back()->withInput()->with('flash_message_error', 'Parent update unsuccessful, Please tey again!');
            }
        \DB::commit();
        return redirect('parents')->with('flash_message_success', 'Parent updated successfully!');
        // flash_message_error // flash_message_info
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        
        \DB::beginTransaction();
        // 
            try {
                $parent = \App\Parents::find($id);
                $parent->delete(); 
            } catch (Exception $es) {
                \DB::rollBack();
                return redirect('')->back()->with('flash_message_error', 'Parent delete unsuccessful, Please tey again!');
            }
        \DB::commit();
        return redirect('parents')->with('flash_message_success', 'Parent deleted successfully!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _ajaxData()
    {
        $parents = \App\Parents::select('id', 'name', 'dob', 'status') 
            ->get();

        foreach ($parents as $k => $parent):
            // row id
            $parent->row = $k+1;
        endforeach;

        return response()->json(['parents' => $parents]);
    }
}
