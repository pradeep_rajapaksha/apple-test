<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Classs; 

class ClassController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
        return view('classes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        // 
        $classes = \App\Classs::all()->pluck('name', 'id');
        $parents = \App\Parents::all()->pluck('name', 'id');
        return view('classes.create', compact('classes', 'parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        //
        $this->validate($request, \App\Classs::$rules);
        
        $class = new \App\Classs();
        $class->name = $request->name;
        $class->year = $request->year;
        if ($class->save()) {
            // 
            return redirect('classes')->with('flash_message_success', 'Classs created successfully!');
        }
        
        return redirect()->back()->withInput()->with('flash_message_error', 'Classs couldn`t create successfully! Please chack entered data.');
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $class = \App\Classs::findOrFail($id);
        // dd($class->students);
        return view('classes.show', compact('class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 

        $class = \App\Classs::findOrFail($id);
        $classes = \App\Classs::all()->pluck('name', 'id'); 
        $parents = \App\Parents::all()->pluck('name', 'id');

        return view('classes.edit', compact('class', 'classes', 'parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        // 
        \DB::beginTransaction();
            try { 
                $class = \App\Classs::find($id);
                $class->name = $request->name;
                $class->year = $request->year;
                $class->save();
                // 
                
            } catch (Exception $e) { 
                \DB::rollBack(); 
                return redirect('')->back()->withInput()->with('flash_message_error', 'Classs update unsuccessful, Please tey again!');
            }
        \DB::commit();
        return redirect('classes')->with('flash_message_success', 'Classs updated successfully!');
        // flash_message_error // flash_message_info
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        \DB::beginTransaction();
        // 
            try {
                $class = \App\Classs::find($id);
                $class->delete(); 
            } catch (Exception $es) {
                \DB::rollBack();
                return redirect('')->back()->with('flash_message_error', 'Classs delete unsuccessful, Please tey again!');
            }
        \DB::commit();
        return redirect('classes')->with('flash_message_success', 'Classs deleted successfully!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _ajaxData()
    {
        $classes = \App\Classs::select('classes.id', 'classes.name', 'classes.year') 
            ->get();
        foreach ($classes as $k => $class):
            // row id
            $class->students_count = count($class->students);
            $class->row = $k+1;
        endforeach;

        return response()->json(['classes' => $classes]);
    }
}
