<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Student; 

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
        return view('students.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        // 
        $classes = \App\Classs::all()->pluck('name', 'id');
        $parents = \App\Parents::all()->pluck('name', 'id');
        return view('students.create', compact('classes', 'parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        //
        $this->validate($request, \App\Student::$rules);
        
        $student = new \App\Student();
        $student->name              = $request->name;
        $student->class_id          = $request->class_id;
        $student->dob               = $request->dob;
        $student->address           = $request->address;
        $student->city              = $request->city;
        $student->city_coordinates  = ($request->city_coordinates) ? $request->city_coordinates : '' ;
        if ($student->save()) {
            // 
            $parent = \App\Parents::find($request->parent_id);
            if (! $student->parent->contains($parent)) {
                $student->parent()->save($parent);
            }
            return redirect('students')->with('flash_message_success', 'Student created successfully!');
        }
        
        return redirect()->back()->withInput()->with('flash_message_error', 'Student couldn`t create successfully! Please chack entered data.');
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $student = \App\Student::findOrFail($id);
        // dd($student->parent);
        return view('students.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 

        $student = \App\Student::findOrFail($id);
        $classes = \App\Classs::all()->pluck('name', 'id'); 
        $parents = \App\Parents::all()->pluck('name', 'id');

        $student->parent_id = 0;
        if (!empty($student->parent)) {
            $student->parent_id = $student->parent[0]->id; 
        }
        // 
        return view('students.edit', compact('student', 'classes', 'parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        // 
        \DB::beginTransaction();
            try { 
                $student = \App\Student::find($id);
                $student->name              = $request->name;
                $student->class_id          = $request->class_id;
                $student->dob               = $request->dob;
                $student->address           = $request->address;
                $student->city              = $request->city;
                $student->city_coordinates  = ($request->city_coordinates) ? $request->city_coordinates : '' ;
                $student->save();
                // 
                $parent = \App\Parents::find($request->parent_id);
                if (! $student->parent->contains($parent)) {
                    $student->parent()->save($parent);
                }

            } catch (Exception $e) { 
                \DB::rollBack(); 
                return redirect('')->back()->withInput()->with('flash_message_error', 'Student update unsuccessful, Please tey again!');
            }
        \DB::commit();
        return redirect('students')->with('flash_message_success', 'Student updated successfully!');
        // flash_message_error // flash_message_info
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // check role 
        if ( !\Auth::user()->authorizeRoles(['admin']) ) { 
            return redirect()->to('/')->with('flash_message_error', 'This action is unauthorized.');
        } 
        
        \DB::beginTransaction();
        // 
            try {
                $student = \App\Student::find($id);
                $student->delete(); 
            } catch (Exception $es) {
                \DB::rollBack();
                return redirect('')->back()->with('flash_message_error', 'Student delete unsuccessful, Please tey again!');
            }
        \DB::commit();
        return redirect('students')->with('flash_message_success', 'Student deleted successfully!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _ajaxData()
    {
        $students = \App\Student::select('students.id', 'students.name', 'students.class_id', 'students.city', 'students.city_coordinates', 'classes.name as class', 'classes.year') 
            ->join('classes', 'students.class_id', '=', 'classes.id')
            ->get();

        foreach ($students as $k => $student):
            // row id
            $student->row = $k+1;
        endforeach;

        return response()->json(['students' => $students]);
    }
}
