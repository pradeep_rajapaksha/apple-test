<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _ajaxReport1(Request $request)
    {
    	$agemax = ($request->agemax) ? $request->agemax : 0; 
    	$agemin = ($request->agemin) ? $request->agemin : 0; 

    	$students = \App\Student::select('students.id', 'students.name', 'students.dob', 'students.class_id', 'students.city', 'students.city_coordinates', 'classes.name as class', 'classes.year') 
            ->join('classes', 'students.class_id', '=', 'classes.id')
            ->get();

        foreach ($students as $k => $student):
            // row id
            $student->row = $k+1;
            $age = \Carbon\Carbon::parse($student->dob)->diff(\Carbon\Carbon::now())->format('%y');

            // check min 
            if ( $agemin != 0 and $agemin > $age ) {
            	unset($students[$k]);
            }
            // check max 
            if ( $agemax != 0 and $agemax <= $age ) {
            	unset($students[$k]);
            }
            $student->age = $age;
        endforeach;

        return response()->json(['students' => $students]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _ajaxReport2(Request $request)
    {
    	$class = ($request->class) ? $request->class : 0; 
    	$year = ($request->year) ? $request->year : date('Y'); 

    	$students = \App\Student::select('students.id', 'students.name', 'students.class_id', 'students.city', 'students.city_coordinates', 'classes.name as class', 'classes.year') 
            ->join('classes', 'students.class_id', '=', 'classes.id')
            ->where('classes.name', 'like', '%'.$class)
            ->where('classes.year', $year)
            ->get();

        foreach ($students as $k => $student):
            // row id
            $student->row = $k+1;
        endforeach;

        return response()->json(['students' => $students]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _ajaxReport3(Request $request)
    {
    	$agestudent = ($request->agestudent) ? $request->agestudent : 0; 
    	$ageparent = ($request->ageparent) ? $request->ageparent : 0; 

    	$students = \App\Student::select('students.id', 'students.name', 'students.dob', 'students.class_id', 'classes.name as class', 'classes.year') 
            ->join('classes', 'students.class_id', '=', 'classes.id')
            ->get();

        foreach ($students as $k => $student):
            // row id
            $student->row = $k+1;
            $student->parent_name = $student->parent[0]->name; 	

            // age of student
	            $_age_std = \Carbon\Carbon::parse($student->dob)->diff(\Carbon\Carbon::now())->format('%y');
	            // check age student 
	            if ( $agestudent != 0 and $agestudent > $_age_std ) {
	            	unset($students[$k]);
	            }
	            $student->agestudent = $_age_std;

            // age of parent
	            $_age_prt = \Carbon\Carbon::parse($student->parent[0]->dob)->diff(\Carbon\Carbon::now())->format('%y');
	            // check age student 
	            if ( $ageparent != 0 and $ageparent > $_age_prt ) {
	            	unset($students[$k]);
	            }
	            $student->ageparent = $_age_prt;

        endforeach;

        return response()->json(['students' => $students]);
    }

}
