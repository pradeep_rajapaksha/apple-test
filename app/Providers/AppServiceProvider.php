<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        //
        Schema::defaultStringLength(191);

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $event->menu->add('MAIN NAVIGATION');
            $event->menu->add([
                'text'  => 'Dashboard',
                'url'   => '/',
                'icon'  => 'tachometer',
                'active' => ['dashboard']
            ]);
            $event->menu->add([
                'text'  => 'Students',
                'url'   => '/students',
                'icon'  => 'user-circle-o',
                // 'icon_color' => 'aqua',
                'active' => ['students', 'students/*']
            ]);
            $event->menu->add([
                'text'  => 'Parents',
                'url'   => '/parents',
                'icon'  => 'users',
                // 'icon_color' => 'aqua',
                'active' => ['parents', 'parents/*']
            ]);
            $event->menu->add([
                'text'  => 'Classes',
                'url'   => '/classes',
                'icon'  => 'graduation-cap',
                // 'icon_color' => 'aqua',
                'active' => ['classes', 'classes/*']
            ]);

            // 
            $event->menu->add('SETTINGS AND ADMINISTRATION'); 
            $event->menu->add([
                'text' => 'My Profile',
                'url'  => '/profile',
                'icon'  => 'user',
            ]);
            if (auth()->user()->hasRole('admin')) {
                # code...
                $event->menu->add([
                    'text' => 'Staff',
                    'url'  => '/staff',
                    'icon'  => 'users',
                ]);
                $event->menu->add([
                    'text' => 'System Config',
                    'url'  => '/',
                    'icon'  => 'cog',
                    'active' => ['config', 'config/*']
                ]);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
