<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'class_id', 'dob', 'address', 'city', 'city_coordinates'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be valid for create.
     *
     * @var array
     */
    public static $rules = [
        'name'			=> 'required',
        'class_id'     	=> 'required',
        'dob'     		=> 'required|date',
        'address'     	=> 'required',
        'city'          => 'required',
        'city_coordinates'=> '',
        'parent_id'     => 'required',
    ];

    /**
     * The attributes that should be valid for create.
     *
     * @var array
     */
    public static $message = [];

    /**
     * Get the class record associated with the user.
     */
    public function class()
    {
        return $this->hasOne('App\Classs', 'id', 'class_id');
    }

    /**
     * The parent that belong to the user.
     */
    public function parent()
    {
        return $this->belongsToMany('App\Parents', 'student_parent', 'student_id', 'parent_id'); 
    }
}
