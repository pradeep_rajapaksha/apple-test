<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classs extends Model
{
    //
    use SoftDeletes;


    /**
     * The attributes that is table name.
     *
     * @var string
     */
    protected $table = 'classes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'year', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be valid for create.
     *
     * @var array
     */
    public static $rules = [
        'name'		=> 'required',
        'year'		=> 'required|digits:4',
        'status'	=> ''
    ];

    /**
     * The attributes that should be valid for create.
     *
     * @var array
     */
    public static $message = [];

    /**
     * Get the students for the blog post.
     */
    public function students()
    {
        return $this->hasMany('App\Student', 'class_id', 'id'); 
    }
}
