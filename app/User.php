<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    // 
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be valid for create.
     *
     * @var array
     */
    public static $rules = [
        'name'=> 'required',
        'email'     => 'required|string|email|max:191|unique:users',
        'password'  => 'required|between:6,50',
    ];

    /**
     * The attributes that should be valid for create.
     *
     * @var array
     */
    public static $message = [
        'mobile' => 'The mobile field must be 10 characters.',
        'telephone' => 'The telephone field must be 10 characters.',
    ];

    /**
     * The user that belong to the role.
     */
    public function roles() {
      return $this->belongsToMany('\App\Role');
    }

    /**
     * @param string|array $roles
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return ($this->hasAnyRole($roles) AND in_array('admin', $roles)) || 
                false; 
        }
        return ($this->hasRole($roles) AND in_array('admin', $roles)) || 
            false; 
    }

    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
      return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     * @param string $role
     */
    public function hasRole($role)
    {
      return null !== $this->roles()->where('name', $role)->first();
    }
}
