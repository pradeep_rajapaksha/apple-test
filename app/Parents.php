<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parents extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'dob', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be valid for create.
     *
     * @var array
     */
    public static $rules = [
        'name'		=> 'required',
        'type'     	=> 'required',
        'dob'     	=> 'required|date',
        'status'	=> '',
    ];

    /**
     * The attributes that should be valid for create.
     *
     * @var array
     */
    public static $message = [];

    /**
     * Get the childs for the blog post.
     */
    public function childs()
    {
        return $this->belongsToMany('App\Student', 'student_parent', 'parent_id', 'student_id'); 
    }

}
